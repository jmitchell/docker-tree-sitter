#!/usr/bin/env bash

declare -a branches=("v0.13.10" "master")

for b in "${branches[@]}"
do
    docker build --build-arg branch="${b}" --tag "tree-sitter-cli:${b}" .
done
