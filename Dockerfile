FROM node:8.12.0-jessie
ARG branch

RUN apt-get update
RUN apt-get install -y clang-4.0 build-essential

USER node
ENV CXX=clang++-4.0
ENV HOME=/home/node
RUN mkdir $HOME/.npm-global
RUN npm config set prefix $HOME/.npm-global
ENV PATH=$HOME/.npm-global/bin:$PATH

RUN git clone --recurse-submodules --branch ${branch} https://github.com/tree-sitter/tree-sitter-cli.git $HOME/tree-sitter-cli
WORKDIR $HOME/tree-sitter-cli
RUN npm install
RUN npm test || true
RUN npm -g install .
